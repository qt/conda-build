FROM continuumio/miniconda
MAINTAINER Joseph Weston <j.b.weston@tudelft.nl>

RUN apt-get update && \
    apt-get install --yes build-essential lsb-release

RUN conda config --add channels conda-forge && \
    conda install --yes conda-build anaconda-client
